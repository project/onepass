<?php

namespace Drupal\onepass\Plugin\views\style;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Default style plugin to render OnePass atoms feed.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "onepassatoms",
 *   title = @Translation("OnePass atoms feed"),
 *   help = @Translation("Generates OnePass Atoms feed from a view."),
 *   theme = "views_view_onepassatoms",
 *   display_types = {"feed"}
 * )
 */
class OnepassAtomsFeed extends StylePluginBase {

  /**
   * Onepass service.
   *
   * @var \Drupal\onepass\OnepassServiceInterface
   */
  protected $onepass;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // @TODO: ReDo it in a more proper way.
    $this->onepass = \Drupal::service('onepass.service');
    $this->time = \Drupal::time();
  }

  /**
   * {@inheritdoc}
   */
  public function attachTo(array &$build, $display_id, Url $feed_url, $title) {
    $display = $this->view->displayHandlers->get($display_id);
    $url_options = [];
    $input = $this->view->getExposedInput();
    if ($input) {
      $url_options['query'] = $input;
    }
    $url_options['absolute'] = TRUE;

    $url = $feed_url->setOptions($url_options)->toString();
    if ($display->hasPath()) {
      if (empty($this->preview)) {
        $build['#attached']['feed'][] = [$url, $title];
      }
    }
    else {
      $this->view->feedIcons[] = [
        '#theme' => 'feed_icon',
        '#url' => $url,
        '#title' => $title,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];

    foreach ($this->view->result as $row) {
      if (isset($row->nid)) {
        $node = Node::load($row->nid);

        if ($node instanceof NodeInterface) {
          $rows[] = [
            'title' => $node->label(),
            'link' => $node->toUrl(
              'canonical',
              ['absolute' => TRUE]
            )->toString(),
            'summary' => $this->getNodeDisplay(
              $this->onepass->markForTrim($node)
            ),
            'content' => $this->getNodeDisplay(
              $node
            ),
            'id' => $this->onepass->getShortCodeReplacementUniqueId(
              $node->id()
            ),
            'updated' => $this->onepass->formatDate(
              $node->getChangedTime()
            ),
            'published' => $this->onepass->formatDate(
              $node->getCreatedTime()
            ),
            'author' => [
              'name' => $node->getOwner()->getDisplayName(),
              'email' => $node->getOwner()->getEmail(),
            ],
          ];
        }
      }
    }

    $this->options['updated'] = $this->onepass->formatDate($this->time->getRequestTime());
    $this->options['pagination'] = $this->view->pager->render([]);
    $this->options['feed_id'] = $this->onepass->getShortCodeReplacementUniqueId(1);

    return [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#rows' => $rows,
    ];
  }

  /**
   * Prepare node for display.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   *
   * @return array
   *   Node rendering array.
   */
  private function getNodeDisplay(NodeInterface $node) {
    $controller = \Drupal::entityTypeManager()->getViewBuilder($node->getEntityTypeId());
    $display = $controller->view($node, 'full');
    $display['#theme'] = 'onepassatoms_node';
    return $display;
  }

}
