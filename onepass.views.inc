<?php

/**
 * @file
 * Provides views data for onepass.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function onepass_views_data_alter(&$data) {
  $data['node']['onepass_relation'] = [
    'title' => t('OnePass relation'),
    'help' => t('Relation to OnePass service marked nodes.'),
    'relationship' => [
      'id' => 'standard',
      'base' => 'onepass_node',
      'base field' => 'nid',
      'field' => 'nid',
      'label' => t('OnePass relation'),
      'title' => t('OnePass relation'),
      'help' => t('Relation to OnePass service marked nodes.'),
    ],
  ];
}
